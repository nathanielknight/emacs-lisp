# Insert a Comment Heading

Some programmers like to split up their programs with comment headings

    # Like this -----------------------------------------------------------------

    ;;==============================================================================
    ;; Or This

    /* ******************************************************
       *       OR EVEN THIS                                 *
       ****************************************************** */

depending on their several tendencies.

Write an Emacs function which will prompt the user for a title and insert an appropriate heading in your preferred style. You'll need to check out the [interactive](http://www.gnu.org/software/emacs/manual/html_node/elisp/Using-Interactive.html) special form.

Extra points for:

* Accepting a template from the user so they can define their own heading. 
* Offering different levels of heading (perhaps based on a [prefix argument](https://www.gnu.org/software/emacs/manual/html_node/elisp/Prefix-Command-Arguments.html)).
* Inferring how to comment out the heading based on the users' Major Mode.
