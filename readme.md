# Emacs Lisp Exercises

This repo has exercises to get you started programming Emacs Lisp. If you want a quick overview of the syntax, you can browser through [Learn Emacs Lisp in Y Minutes](http://learnxinyminutes.com/docs/elisp/) in about 15 minutes.

1. [Inserting Text](elisp-challenge1.md)
3. [Executing Interactively](elisp-challenge2.md)
2. [A Minor Mode](elisp-challenge3.md)

A few other ideas (for the contrary, the Emacs Lisp veterans, or the contributors-to-open-source):

* Check out the behaviour of `backward-kill-word` in Clojure mode with Paredit mode. (In particular, kill the last work in an S-expression). Do you think that's right? Here's the [source](http://mumble.net/~campbell/git/paredit.git/).


## Emacs Commands for Finding Functions

* `C-h k` What function does this key-combination execute?
* `C-h w` What key-combination executes this function?
* `C-h f` Show me the docs for this function
* `C-h v` Show me the docs for this variable
