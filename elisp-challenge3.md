# Simple Wikipedia Minor Mode

[Simple Wikipedia](https://simple.wikipedia.org/wiki/Main_Page) is the world's largest English-language community constructed encyclopedia which strives to use only the thousand most common English words. Write a minor mode for composing text which highlights words that aren't in the thousand most common.

Extra points for being able to :
* Change the size of your vocabulary. (be in the two-thousand most common, or the five-hundred most common, etc.).
* Change languages (French, Swedish, Pirate, etc.).
* Offering a thesaurus to help the writer find a replacement for complicated.
* Optionally adding the thousand most common *programming* words.

You'll want to check out `define-derived-mode` or `define-minor-mode` to get started.
