# Google My Region

The `browse-url` function lets you launch a web browser pointed at the specified URL. Use it to write a function that Google's text highlighted in the editor (called the `region` in the Emacs Lisp docs).

Extra points for adapting your program to

* Giving the user the choice of a few search engines.
* Search the documentation for your favourite functional programming language.
* Search the posts of your favourite social media site (woo scraping in Elisp!).
