(defun org-next-slide ()
  (interactive)
  (beginning-of-buffer)
  (forward-char)
  (widen)
  (if (search-forward-regexp
	   (rx line-start "* ") nil t)
	  (progn
		(org-narrow-to-subtree)
		(show-all))
	(progn
	  (org-narrow-to-subtree)
	  (beginning-of-buffer)	  
	  (message "Last slide"))))

(defun org-prev-slide ()
  (interactive)
  (beginning-of-buffer)
  (widen)
  (if (search-backward-regexp
	   (rx line-start "* ") nil t)
	  (progn
		(org-narrow-to-subtree)
		(show-all))
	(progn
	  (org-narrow-to-subtree)
	  (beginning-of-buffer)
	  (message "First slide"))))

(defvar org-slides-mode-keymap
  (let ((map (make-sparse-keymap)))
	(define-key map (kbd "<next>") 'org-next-slide)
	(define-key map (kbd "<prior>") 'org-prev-slide)
	map))

;; keymap 

(define-minor-mode org-slides-mode
  "View org-mode sub-trees as slides."
  :lighter " Slides"
  :keymap org-slides-mode-keymap
  (progn
	(set-face-attribute 'default nil :height 300)	
	(set-variable 'left-margin-width '2 t)
	(set-window-buffer (selected-window) (current-buffer))))



